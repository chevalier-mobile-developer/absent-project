package es.esy.hallohai.absentproject.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import es.esy.hallohai.absentproject.R;
import es.esy.hallohai.absentproject.base.BaseActivity;
import es.esy.hallohai.absentproject.data.ListMahasiswa;
import es.esy.hallohai.absentproject.data.Mahasiswa;
import es.esy.hallohai.absentproject.ui.addmahasiswa.AddMahasiswaActivity;

public class MainActivity extends BaseActivity {

    private RecyclerView recyclerViewMahasiswa;
    private FloatingActionButton buttonAddMahasiswa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialitation();
        buttonListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setDataToListMahasiswa();
    }

    /**
     * digunakan untuk inisialisasi data dari interface
     */
    private void initialitation(){
        recyclerViewMahasiswa = findViewById(R.id.list_mahasiswa);
        buttonAddMahasiswa = findViewById(R.id.button_add_mahasiswa);
    }

    /**
     * untuk mendeklarasikan saat button di click
     */
    private void buttonListener(){
        buttonAddMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAddMahasiswa();
            }
        });
    }

    /**
     * untuk pindah ke halaman AddMahasiswa
     */
    private void goToAddMahasiswa(){
        Intent intent = new Intent(MainActivity.this, AddMahasiswaActivity.class);
        startActivity(intent);
    }

    /**
     * untuk menampilkan data pada list mahasiswa
     */
    private void setDataToListMahasiswa(){
        ListMahasiswa listMahasiswa = new ListMahasiswa();
        ArrayList<Mahasiswa> mahasiswas = listMahasiswa.getMahasiswas();
        Mahasiswa[] arrMahasiswas = new Mahasiswa[mahasiswas.size()];

        int i = 0;
        for (Mahasiswa mahasiswa: mahasiswas) {
            arrMahasiswas[i] = mahasiswa;
            i++;
        }

        recyclerViewMahasiswa.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerViewMahasiswa.setLayoutManager(mLayoutManager);

        RecyclerView.Adapter mAdapter = new ListAdapter(arrMahasiswas);
        recyclerViewMahasiswa.setAdapter(mAdapter);
    }
}
