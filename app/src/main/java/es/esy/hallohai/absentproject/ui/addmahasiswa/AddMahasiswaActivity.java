package es.esy.hallohai.absentproject.ui.addmahasiswa;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import es.esy.hallohai.absentproject.R;
import es.esy.hallohai.absentproject.base.BaseActivity;
import es.esy.hallohai.absentproject.data.ListMahasiswa;
import es.esy.hallohai.absentproject.data.Mahasiswa;

public class AddMahasiswaActivity extends BaseActivity {

    private EditText editTextName;
    private EditText editTextNim;
    private Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mahasiswa);

        initialitation();
        buttonListener();
    }

    /**
     * digunakan untuk inisialisasi data dari interface
     */
    private void initialitation(){
        editTextName = findViewById(R.id.input_name);
        editTextNim = findViewById(R.id.input_nim);
        buttonSubmit = findViewById(R.id.button_submit);
    }

    /**
     * untuk mendeklarasikan saat button di click
     */
    private void buttonListener(){
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDataToList(getDataField());
            }
        });
    }

    private Mahasiswa getDataField(){
        String name = editTextName.getText().toString().trim();
        String nim = editTextNim.getText().toString().trim();
        return new Mahasiswa(name, nim);
    }

    private void addDataToList(Mahasiswa mahasiswa){
        ListMahasiswa listMahasiswa = new ListMahasiswa();
        listMahasiswa.addMahasiswa(mahasiswa);
        finish();
    }
}
