package es.esy.hallohai.absentproject.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created with love by Hari Nugroho on 27/03/2018 at 23.12.
 */

public class BaseActivity extends AppCompatActivity{

    private AppCompatActivity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;
    }
}
