package es.esy.hallohai.absentproject.data;

import java.util.ArrayList;

/**
 * Created with love by Hari Nugroho on 27/03/2018 at 23.23.
 */

public class ListMahasiswa {

    private static ArrayList<Mahasiswa> mahasiswas = new ArrayList<>();

    public void addMahasiswa(Mahasiswa mahasiswa){
        mahasiswas.add(mahasiswa);
    }

    public ArrayList<Mahasiswa> getMahasiswas() {
        return mahasiswas;
    }

    public void setMahasiswas(ArrayList<Mahasiswa> mahasiswas) {
        ListMahasiswa.mahasiswas = mahasiswas;
    }
}
