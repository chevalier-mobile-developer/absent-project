package es.esy.hallohai.absentproject.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import es.esy.hallohai.absentproject.R;
import es.esy.hallohai.absentproject.data.Mahasiswa;

/**
 * Created with love by Hari Nugroho on 27/03/2018 at 23.57.
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private Mahasiswa[] mDataset;

    public ListAdapter(Mahasiswa[] mDataset) {
        this.mDataset = mDataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView viewName = holder.mItemView.findViewById(R.id.view_name);
        TextView viewNim = holder.mItemView.findViewById(R.id.view_nim);

        viewName.setText(mDataset[position].getNama());
        viewNim.setText(mDataset[position].getNim());
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mItemView;
        public ViewHolder(View v) {
            super(v);
            mItemView = v;
        }
    }
}
